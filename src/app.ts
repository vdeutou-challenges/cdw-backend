import express from "express";
import { graphqlHTTP } from "express-graphql";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import { buildSchema } from "graphql";
import User from "./models/User";

dotenv.config();

const app = express();
const PORT = process.env.PORT || 4000;

app.use(cors());

const mongooseOptions: any = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

mongoose
  .connect(
    "mongodb+srv://root:root@ocenture.kaaeb5h.mongodb.net/TestDB",
    mongooseOptions
  )
  .then(() => {
    console.log("Connected to MongoDB");
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.error("Error connecting to MongoDB:", error);
  });

const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

const schema = buildSchema(`
  type User {
    id: ID
    firstName: String
    lastName: String
    email: String
  }

  input CreateUserInput {
    firstName: String
    lastName: String
    email: String
  }

  type Query {
    users: [User]
  }

  type Mutation {
    createUser(input: CreateUserInput): User
  }
`);

const root = {
  users: () => User.find(),
  createUser: async (args: {
    input: { firstName: string; lastName: string; email: string };
  }) => {
    const { firstName, lastName, email } = args.input;
    const user = new User({ firstName, lastName, email });
    await user.save();
    return user;
  },
};

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);
